#!/usr/bin/env python
"""
unsplash-download - Downloads images from unsplash.com.

Usage:
  unsplash-download <folder>
  unsplash-download <folder> [<profile>] [--search=<keyword>] [--collection=<collection>] [--multithread]
  unsplash-download -h | --help
  unsplash-download -v | --version

Options:
  -h --help                 Show this screen
  -v --version              Show version

"""
import os
import re
import sys
import _thread
import urllib.request

from docopt import docopt, DocoptExit


try:
    from bs4 import BeautifulSoup, SoupStrainer
except ImportError as e:
    print("Could not import beatifulsoup4. Make sure it is installed.")
    if DEBUG:
      print(e)
    sys.exit()

DEBUG = False
ud_version = '1.0.5'
arguments = docopt(
    __doc__, help=True, version='unsplash-download ' + ud_version
)
download_path = arguments['<folder>']
base_url = 'https://unsplash.com'
page = 1
link_search = re.compile("/photos/[a-zA-Z0-9-_]+/download")

if not os.path.exists(download_path):
    os.makedirs(download_path)

while True:
    url = base_url + "/?page=" + str(page)
    if arguments['<profile>']:
        url = base_url + "/%s" % arguments['<profile>'] + "/?page=" + str(page)
    if arguments['--search']:
        url = base_url + "/search?keyword=%s" % arguments['--search']
    if arguments['--collection']:
        url = base_url + "/collections/%s" % arguments['--collection']
    print("Parsing page %s\n" % url)
    try:
        soup = BeautifulSoup(urllib.request.urlopen(url).read(), "lxml")
        download_links = set(re.findall(link_search, str(soup)))

        # Exit if no images found
        if len(download_links) == 0:
          print("Your search returned no results. Exiting")
          break
        else:
          print("Found %s images\n" % len(download_links))

        for tag in download_links:
            download_url = str(base_url + tag)
            image_id = download_url.split('/')[4]

            if os.path.exists("%s/%s.jpeg" % (download_path, image_id)):
                print("Not downloading duplicate %s" % download_url)
                continue

            print("Downloading %s" % download_url)

            if arguments['--multithread']:
                _thread.start_new_thread(
                    urllib.request.urlretrieve, (
                        download_url,
                        "%s/%s.jpeg" % (download_path, image_id))
                )
            else:
                urllib.request.urlretrieve(
                    download_url, "%s/%s.jpeg" % (download_path, image_id)
                )

    except urllib.error.HTTPError as e:
        print("HTML error. This would be all.")
        if DEBUG:
            print(e)
        break
    except HTMLParser.HTMLParseError as e:
        print('Error parsing the HTML')
        if DEBUG:
            print(e)
    except:
        print("An unknown error occured")
    finally:
        page = page + 1
